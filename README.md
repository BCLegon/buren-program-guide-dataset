# Buren Program Guide Dataset

Dataset with episode images and Dutch descriptions for the Australian soap opera Neighbours (NL: Buren).

The data was gathered from the online [VRT NU platform](https://www.vrt.be/vrtnu/) of Flemish public television VRT in June 2020.


## Loading the csv file

```python
import pandas as pd

df = pd.read_csv(
    './data.csv',
    header=None,
    names=['episode', 'title', 'summary', 'image']
)
```


## Loading the images

Using the DataFrame `df` loaded above:

```python
from PIL import Image

for _, item in df.iterrows():
    image_name = './images/' + str(int(item.episode)) + '.jpg'
    img = Image.open(image_name)
    # Do something with img here
    # For example img.show()
```
