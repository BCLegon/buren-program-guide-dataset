import pandas as pd
import requests

df = pd.read_csv('./data.csv', header=None, names=['episode', 'title', 'summary', 'image'])

for _, item in df.iterrows():
    print('Downloading image for episode', item.episode)

    # Download image
    image_name = './images/' + str(int(item.episode)) + '.jpg'
    response = requests.get(item.image)
    if response.status_code == 200:
        with open(image_name, 'wb') as f:
            f.write(response.content)
    else:
        print('Download failed:', response.status_code)
