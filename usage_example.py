import pandas as pd
from PIL import Image


# Loading the csv file
df = pd.read_csv(
    './data.csv',
    header=None,
    names=['episode', 'title', 'summary', 'image']
)

# Loading the images
for i, item in df.iterrows():
    image_name = './images/' + str(int(item.episode)) + '.jpg'
    img = Image.open(image_name)
    if i < 10:
        img.show()
